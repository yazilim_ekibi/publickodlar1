using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MysqlEFDenemesi.Models;
using MysqlEFDenemesi.Siniflar;
using MySql.Data.MySqlClient;

namespace MysqlEFDenemesi
{
    public partial class Default : System.Web.UI.Page
    {

        Stopwatch sw = new Stopwatch();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Stopwatch sw1 = new Stopwatch();

            sw1.Start();

            GenericRepository<testtablo> repository = new GenericRepository<testtablo>();
            for (int i = 0; i < 100; i++)
            {
                testtablo tablo = new testtablo();
                tablo.Icerik = $"içerik no {i}";
                tablo.Tarih = DateTime.Now;
                repository.Add(tablo);
            }

            repository.Save();

            sw1.Stop();
            lblSonuc.Text = "100 kayıt toplam " + sw1.ElapsedMilliseconds.ToString() + " milisaniyede eklendi";


        }

        protected void btnKacKayitVar_Click(object sender, EventArgs e)
        {
            sw.Start();

            int kayitSayisi = new GenericRepository<testtablo>().GetAll().ToList().Count;

            sw.Stop();

            lblSonuc.Text = $"{kayitSayisi} kayıt " + sw.ElapsedMilliseconds.ToString() + " milisaniyede okundu";
        }

        protected void btn100KayitEkle_Click(object sender, EventArgs e)
        {
            sw.Start();
            GenericRepository<testtablo> repository = new GenericRepository<testtablo>();
            for (int i = 0; i < 1000; i++)
            {
                testtablo tablo = new testtablo();
                tablo.Icerik = $"içerik no {i}";
                tablo.Tarih = DateTime.Now;
                repository.Add(tablo);
            }

            repository.Save();
            sw.Stop();
            lblSonuc.Text = "1000 kayıt toplam " + sw.ElapsedMilliseconds.ToString() + " milisaniyede eklendi";
        }

        protected void btnAralikSil_Click(object sender, EventArgs e)
        {
            int baslangicDeger = Convert.ToInt32(TextBox1.Text);
            int kacKayitOkunacak = Convert.ToInt32(TextBox2.Text);

            sw.Start();

            GenericRepository<testtablo> repository = new GenericRepository<testtablo>();
            for (int i = baslangicDeger; i < baslangicDeger+kacKayitOkunacak; i++)
            {
                repository.Delete(repository.FindQueryable(a=>a.Id == i).FirstOrDefault());
            }

            repository.Save();
            sw.Stop();
            lblSonuc.Text = $"{baslangicDeger} ' den başlanıp sonraki {kacKayitOkunacak} kayıt " + sw.ElapsedMilliseconds.ToString() + " milisaniyede silindi";
        }

        protected void btnExcmndIleSil_Click(object sender, EventArgs e)
        {
            int baslangicDeger = Convert.ToInt32(TextBox1.Text);
            int kacKayitOkunacak = Convert.ToInt32(TextBox2.Text);

            sw.Start();

            GenericRepository<testtablo> repository = new GenericRepository<testtablo>();

            testtablo tablo = new testtablo();
            for (int i = baslangicDeger; i < baslangicDeger + kacKayitOkunacak; i++)
            {
                tablo.Id = i;
                MySqlParameter sqlParameter = new MySqlParameter("@Id", i);
                MySqlParameter sqlParameter2 = new MySqlParameter("@Icerik", " ");
                //repository.Delete("Delete From testtablo WHERE Id=@Id AND Icerik!=@Icerik", new MySqlParameter[]{ sqlParameter, sqlParameter2});
                repository.Delete("Delete From testtablo WHERE Id=@Id AND Icerik!=@Icerik", new MySqlParameter[] { sqlParameter, sqlParameter2 });
            }

            sw.Stop();

            lblSonuc.Text = $"{baslangicDeger} ' den başlanıp sonraki {kacKayitOkunacak} kayıt " + sw.ElapsedMilliseconds.ToString() + " milisaniyede silindi";

            //sw.Start();

            //GenericRepository<testtablo> repository = new GenericRepository<testtablo>();
            //int baslangicDeger = Convert.ToInt32(TextBox1.Text);
            //int kacKayitOkunacak = Convert.ToInt32(TextBox2.Text);

            //testtablo tablo = new testtablo();



            //for (int i = baslangicDeger; i < baslangicDeger + kacKayitOkunacak; i++)
            //{
            //    Dictionary<string, object> veriler = new Dictionary<string, object>();
            //    veriler.Add("@ID",i);
            //    repository.Delete("Delete From testtablo WHERE Id=@ID", veriler);
            //}

            //sw.Stop();
            //lblSonuc.Text = $"{baslangicDeger} ' den başlanıp sonraki {kacKayitOkunacak} kayıt " + sw.ElapsedMilliseconds.ToString() + " milisaniyede silindi";
        }
    }
}