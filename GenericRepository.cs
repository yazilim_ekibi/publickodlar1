using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using MySql.Data.MySqlClient;
using MysqlEFDenemesi.Models;

namespace MysqlEFDenemesi.Siniflar
{
    public class GenericRepository<TItem> where TItem: class
    {
        private DbContext _dbContext;

        public GenericRepository()
        {
            _dbContext = new demodbEntities();
        }
        public IQueryable<TItem> GetAll()
        {
            return _dbContext.Set<TItem>();
        }

        public TItem GetItem()
        {
            return _dbContext.Set<TItem>().FirstOrDefault();
        }

        public IQueryable<TItem> FindQueryable(Expression<Func<TItem, bool>> expression)
        {
            IQueryable<TItem> items = _dbContext.Set<TItem>().Where(expression);
            return items;
        }

        public void Add(TItem item)
        {
            _dbContext.Set<TItem>().Add(item);
        }

        public void Update(TItem item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
        }

        public void Delete(TItem item)
        {
            _dbContext.Set<TItem>().Remove(item);
        }

        public void Delete(string sql, object[] filtre)
        {
            //_dbContext.Database.ExecuteSqlCommand(sql, new MySqlParameter("@Id", 17301));
            _dbContext.Database.ExecuteSqlCommand(sql, filtre);
        }

        public void Delete(string sql, object filtre, bool isMysql)
        {
            //    string a = filtre.GetType().Name.ToString();
            //    string asd = filtre.ToString();
            //    Console.WriteLine( a + " to string : " + asd);
            _dbContext.Database.ExecuteSqlCommand(sql, (MySqlParameter)filtre);
        }

        public void Delete(string sql, Dictionary<string, object> filtre)
        {
            object[] parameters = new object[filtre.Count];
            int i = 0;
            foreach (KeyValuePair<string, object> keyValuePair in filtre)
            {
                parameters[i]=new MySqlParameter(keyValuePair.Key.ToString(), keyValuePair.Value);
                i++;
            }

            _dbContext.Database.ExecuteSqlCommand(sql, parameters);
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}